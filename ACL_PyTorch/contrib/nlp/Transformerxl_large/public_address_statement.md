| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
|开发引入|https://github.com/kimiyoung/transformer-xl.git |getdata.sh |http://mattmahoney.net/dc/enwik8.zip |说明源码出处|
|开发引入|https://github.com/kimiyoung/transformer-xl.git |getdata.sh |https://raw.githubusercontent.com/salesforce/awd-lstm-lm/master/data/enwik8/prep_enwik8.py |说明源码出处|
|开源代码引入|https://github.com/kimiyoung/transformer-xl.git |sample.patch |https://github.com/tensorflow/tensorflow/blob/r1.10/tensorflow/python/ops/candidate_sampling_ops.py |说明源码出处|