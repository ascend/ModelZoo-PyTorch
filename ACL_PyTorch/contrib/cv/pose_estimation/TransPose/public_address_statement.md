| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
|开源代码引入|https://github.com/yangsenius/TransPose.git |TransPose.patch |Bin Xiao (Bin.Xiao@microsoft.com) |邮箱地址|
|开源代码引入|https://github.com/yangsenius/TransPose.git |TransPose.patch |Hanbin Dai (daihanbin.ac@gmail.com) |邮箱地址|
|开源代码引入|https://github.com/yangsenius/TransPose.git |TransPose.patch |Bin Xiao (Bin.Xiao@microsoft.com) |邮箱地址|
|开源代码引入|https://github.com/yangsenius/TransPose.git |TransPose.patch |Hanbin Dai (daihanbin.ac@gmail.com) |邮箱地址|
|开源代码引入|https://github.com/yangsenius/TransPose.git |TransPose.patch |Feng Zhang (zhangfengwcy@gmail.com) |邮箱地址|
|开源代码引入|https://github.com/yangsenius/TransPose.git |transpose_r.py |Sen Yang (yangsenius@seu.edu.cn) |邮箱地址|
|开源代码引入|https://github.com/yangsenius/TransPose.git |transpose_r.py |https://github.com/facebookresearch/detr/blob/master/models/transformer.py |说明源码出处|