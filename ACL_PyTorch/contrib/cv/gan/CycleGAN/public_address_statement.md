| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
|开源代码引入|https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix |CycleGAN.patch |https://pytorch.org/docs/stable/optim.html |说明源码出处|
|开源代码引入|https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix |CycleGAN.patch |WGAN-GP paper https://arxiv.org/abs/1704.00028 |论文地址|
|开源代码引入|https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix |CycleGAN.patch |https://github.com/jcjohnson/fast-neural-style |说明源码出处|