| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
|开源代码引入|https://github.com/open-mmlab/mmdetection.git |ssd_mmdet.diff|[1] https://arxiv.org/abs/1311.2524|论文地址|
|开源代码引入|https://github.com/open-mmlab/mmdetection.git |ssd_mmdet.diff|# Avoid onnx2tensorrt issue in https://github.com/NVIDIA/TensorRT/issues/1134 # noqa: E501|代码背景说明|