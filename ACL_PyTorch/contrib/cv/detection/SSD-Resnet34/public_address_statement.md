| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
|开源代码引入|https://github.com/mlcommons/training_results_v0.7 |ssd.patch| # http://jany.st/post/2017-11-05-single-shot-detector-ssd-from-scratch-in-tensorflow.html|代码背景说明|
|开源代码引入|https://github.com/mlcommons/training_results_v0.7 |ssd.patch|Reference to https://github.com/kuangliu/pytorch-ssd|代码背景说明|
|开源代码引入|https://github.com/mlcommons/training_results_v0.7 |ssd.patch|Reference to https://github.com/amdegroot/ssd.pytorch|代码背景说明|
|开源代码引入|https://github.com/mlcommons/training_results_v0.7 |ssd.patch|https://github.com/open-mmlab/mmdetection/blob/master/mmdet/core/post_processing/bbox_nms.py#L7|代码背景说明|
|开源代码引入|https://github.com/mlcommons/training_results_v0.7 |ssd.patch|raise ImportError("Please install APEX from https://github.com/nvidia/apex")|依赖说明|
|开源代码引入|https://github.com/mlcommons/training_results_v0.7 |ssd.patch|# This function is from https://github.com/kuangliu/pytorch-ssd|代码背景说明|