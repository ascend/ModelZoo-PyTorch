| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地 | 用途说明 |
| ---- | ----------- | ----- | -------------------------------- | --------|
| 开发引入 | / | ACL_PyTorch/contrib/cv/detection/DSFD/DSFD.patch | https://github.com/Hakuyume/chainer-ssd | 说明源码地址 |
| 开发引入 | / | ACL_PyTorch/contrib/cv/detection/DSFD/DSFD.patch | https://github.com/fmassa/object-detection.torch | 说明源码地址 |
| 开发引入 | / | ACL_PyTorch/contrib/cv/detection/DSFD/DSFD.patch | https://arxiv.org/pdf/1512.02325.pdf | 论文链接地址 |
| 开发引入 | / | ACL_PyTorch/contrib/cv/detection/DSFD/DSFD.patch | https://arxiv.org/pdf/1512.02325.pdf | 论文链接地址 |