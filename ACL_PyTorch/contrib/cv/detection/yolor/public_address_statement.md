| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
|开发引入|\ |coco.yaml| # 20k images for submission to https://competitions.codalab.org/competitions/20794|注释说明|
|开源代码引入|https://github.com/WongKinYiu/yolor|yolor.patch| # possible CUDA error https://github.com/ultralytics/yolov3/issues/1139|代码背景说明|
|开源代码引入|https://github.com/WongKinYiu/yolor|yolor.patch| # weighted sum of 2 or more layers https://arxiv.org/abs/1911.09070|论文地址|
|开发引入|\ | # https://github.com/cocodataset/cocoapi/blob/master/PythonAPI/pycocoEvalDemo.ipynb|代码背景说明|