| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
|开发引入|\ |deform_conv.py| https://github.com/open-mmlab/mmcv/blob/master/mmcv/ops/modulated_deform_conv.py|注释说明|
|开发引入|\ |deform_conv.py|[Deformable ConvNets v2: More Deformable, Better Results](https://arxiv.org/abs/1811.11168)|论文地址|
|开源代码引入|https://github.com/dbolya/yolact|YOLACT.patch| # See the bug report here: https://github.com/pytorch/pytorch/issues/17108|代码背景说明|
|开发引入|\ |YOLACT_postprocess.py|https://stackoverflow.com/questions/664014/what-integer-hash-function-are-good-that-accepts-an-integer-hash-key|注释说明|
|开发引入|\ |YOLACT_preprocess.py|https://stackoverflow.com/questions/664014/what-integer-hash-function-are-good-that-accepts-an-integer-hash-key|注释说明|