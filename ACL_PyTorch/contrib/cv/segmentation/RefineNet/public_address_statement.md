| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ----------- | ----- | ---------------------------------- | ------- |
| 开源代码引入 | https://github.com/DrSleep/refinenet-pytorch.git | ACL_PyTorch/contrib/cv/segmentation/RefineNet/RefineNet.patch | vladimir.nekrasov@adelaide.edu.au | license邮箱 |