| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ----------- | ----- | ---------------------------------- | ------- |
| 开源代码引入 | https://github.com/facebookresearch/detectron2 | ACL_PyTorch/contrib/cv/segmentation/Cascade_RCNN/cascadercnn_detectron2.diff | https://github.com/pytorch/pytorch/issues/22812 | 代码背景说明 |