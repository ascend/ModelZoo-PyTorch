torch==1.13.0
diffusers==0.21.0
transformers==4.26.1
open_clip_torch==2.20.0