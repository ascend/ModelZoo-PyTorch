numpy==1.23.0; python_version > '3.7.5'
numpy==1.21.6; python_version == '3.7.5'
torch==2.1.2; python_version > '3.7.5'
torch==1.13.1; python_version == '3.7.5'
torchvision==0.16.2; python_version > '3.7.5'
torchvision==0.14.1; python_version == '3.7.5'
timm==0.4.12
transformers==4.26.1
fairscale==0.4.4
opencv-python==4.8.1.78
onnx==1.15.0; python_version > '3.7.5'
onnx==1.14.1; python_version == '3.7.5'
pycocoevalcap==1.2
onnxsim==0.4.36
onnxruntime==1.15.1; python_version > '3.7.5'
onnxruntime==1.14.1; python_version == '3.7.5'
scipy==1.13; python_version > '3.7.5'
scipy==1.7.3; python_version == '3.7.5'
absl-py==2.1.0
attrs==24.1.0
cloudpickle==3.0.0; python_version > '3.7.5'
cloudpickle==2.2.1; python_version == '3.7.5'
decorator==5.1.1
ml-dtypes==0.4.0; python_version > '3.7.5'
ml-dtypes==0.2.0; python_version == '3.7.5'
psutil==6.0.0
tornado==6.4.1; python_version > '3.7.5'
tornado==6.2; python_version == '3.7.5'