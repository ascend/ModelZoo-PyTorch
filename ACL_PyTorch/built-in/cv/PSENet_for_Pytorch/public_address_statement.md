
| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
|开发引入|/|PSENet_for_Pytorch/url.ini|https://download.pytorch.org/models/resnet18-5c106cde.pth|下载权重|
|开发引入|/|PSENet_for_Pytorch/url.ini|https://download.pytorch.org/models/resnet34-333f7ec4.pth|下载权重|
|开发引入|/|PSENet_for_Pytorch/url.ini|https://download.pytorch.org/models/resnet50-19c8e357.pth|下载权重|
|开发引入|/|PSENet_for_Pytorch/url.ini|https://download.pytorch.org/models/resnet101-5d3mb4d8f.pth|下载权重|
|开发引入|/|PSENet_for_Pytorch/url.ini|https://download.pytorch.org/models/resnet152-b121ed2d.pth|下载权重|
|开发引入|/|fpn_resnet_nearest.py|http://www.apache.org/licenses/|license|
|开发引入|/|fpn_resnet_nearest.py|http://www.apache.org/licenses/LICENSE-2.0|license|
|开发引入|/|Post-processing/Algorithm_DetEva.py|It is slightly different from original algorithm(see https://perso.liris.cnrs.fr/christian.wolf/software/deteval/index.html)|注释说明|
