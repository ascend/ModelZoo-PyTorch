| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
|开发引入|/|MobileNetV2_for_Pytorch/url.ini|https://download.pytorch.org/models/mobilenet_v2-b0353104.pth|下载权重|
|开发引入|/|mobilenet.py|https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py|注释说明|
|开发引入|/|mobilenet.py|`"MobileNetV2: Inverted Residuals and Linear Bottlenecks" <https://arxiv.org/abs/1801.04381>`_.|注释说明|
