按照[《PyTorch框架推理环境准备》](https://www.hiascend.com/document/detail/zh/ModelZoo/pytorchframework/pies/pies_00001.html)准备推理环境，具体的配套表如下：  


  | 配套                                                         | 版本    | 环境准备指导                                                 |
  | ------------------------------------------------------------ | ------- | ------------------------------------------------------------ |
  | 固件与驱动                                                   | [23.0.rc3](https://www.hiascend.com/hardware/firmware-drivers/commercial?product=1&model=30)  | [固件与驱动安装](https://www.hiascend.com/document/detail/zh/canncommercial/700/envdeployment/instg/instg_0019.html) |
  | CANN                                                         | [7.0.0](https://www.hiascend.com/developer/download/commercial/result?module=cann) | [CANN安装](https://www.hiascend.com/document/detail/zh/canncommercial/700/envdeployment/instg/instg_0001.html)                                                        |
  | Python                                                       | [3.10](https://www.python.org/)   | [Pythona安装](https://www.python.org)                                                         |        