| 类型     | 开源代码地址                                                                                                                           | 文件名                                                | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明   |
|--------|----------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------|-----------------------|--------|
| 开发引入 | / | WResNet-28x2_ID1137_for_PyTorch/requirements.txt | https://github.com/wbaek/theconf@de32022f8c0651a043dc812d17194cdfd62066e8 | 相关依赖 |
| 开发引入 | / | WResNet-28x2_ID1137_for_PyTorch/requirements.txt | https://github.com/ildoonet/pytorch-gradual-warmup-lr.git@v0.2 | 相关依赖 |
| 开发引入 | / | WResNet-28x2_ID1137_for_PyTorch/requirements.txt | https://github.com/ildoonet/pystopwatch2.git | 相关依赖 |
