| 类型     | 开源代码地址                                                                                                                           | 文件名                                                | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明   |
|--------|----------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------|-----------------------|--------|
| 开发引入 | / | YOLOX_Dynamic_ID4069_for_PyTorch/1.5_requirements.txt | https://github.com/ppwwyyxx/cocoapi | 相关依赖 |
| 开发引入 | / | YOLOX_Dynamic_ID4069_for_PyTorch/1.8_requirements.txt | https://github.com/ppwwyyxx/cocoapi | 相关依赖 |
| 开发引入 | / | YOLOX_Dynamic_ID4069_for_PyTorch/docs/requirements-doc.txt | https://github.com/sphinx-doc/sphinx/commit/7acd3ada3f38076af7b2b5c9f3b60bb9c2587a3d | 相关依赖 |
