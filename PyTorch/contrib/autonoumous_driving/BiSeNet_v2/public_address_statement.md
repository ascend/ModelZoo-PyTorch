| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
| 开源代码引入 | https://github.com/CoinCheung/BiSeNet/blob/master/lib/models/bisenetv2.py | BiSeNet_v2/lib/models/bisenetv2.py | https://github.com/CoinCheung/BiSeNet/releases/download/0.0.0/backbone_v2.pth | 模型bakcbone地址 |