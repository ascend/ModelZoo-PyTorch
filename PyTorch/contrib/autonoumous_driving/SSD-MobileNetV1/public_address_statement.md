# Public Address Statement

| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
|------|--------|-------------------------| ------------------------------------ |------|
| 开源代码引入 | <https://github.com/qfgaohao/pytorch-ssd/blob/master/vision/nn/mobilenet.py> | SSD-MobileNetV1/vision/nn/mobilenet.py | <https://github.com/marvis/pytorch-mobilenet> | 源码实现 |
| 开源代码引入 | <https://github.com/qfgaohao/pytorch-ssd/blob/master/vision/transforms/transforms.py> | SSD-MobileNetV1/vision/transforms/transforms.py | <https://github.com/amdegroot/ssd.pytorch> | 源码实现 |
| 开源代码引入 | <https://github.com/qfgaohao/pytorch-ssd/blob/master/vision/utils/box_utils.py> | SSD-MobileNetV1/vision/utils/box_utils.py | <https://arxiv.org/abs/1704.04503> | 参考论文地址 |
| 开源代码引入 | <https://github.com/qfgaohao/pytorch-ssd/blob/master/vision/utils/box_utils.py> | SSD-MobileNetV1/vision/utils/box_utils.py | <https://github.com/facebookresearch/Detectron/blob/master/detectron/utils/cython_nms.pyx> | 源码实现 |
