torchvision==0.19.0
Pillow==10.2.0
decorator==5.1.0
sympy==1.9
iopath
fvcore
cloudpickle
