
|   类型   |          开源代码地址           |                                              文件名                                              | 公网IP地址/公网URL地址/域名/邮箱 |       用途说明        |
|:------:|:-------------------------:|:---------------------------------------------------------------------------------------------:|:--------------------:|:-----------------:|
| 开源代码引入 | https://github.com/Kwai-Kolors/Kolors/blob/master/MODEL_LICENSE | ./MODEL_LICENSE | kwai-kolors@kuaishou.com | Model License |
| 开源代码引入 | https://github.com/Kwai-Kolors/Kolors/blob/master/MODEL_LICENSE | ./MODEL_LICENSE | kwai-kolors@kuaishou.com | Mdeol License |
| 开源代码引入 | https://github.com/Kwai-Kolors/Kolors/blob/master/kolors/pipelines/pipeline_stable_diffusion_xl_chatglm_256_inpainting.py | ./kolors/pipelines/pipeline_stable_diffusion_xl_chatglm_256_inpainting.py | https://raw.githubusercontent.com/CompVis/latent-diffusion/main/data/inpainting_examples/overture-creations-5sI6fQgYIuo.png | examples image |
| 开源代码引入 | https://github.com/Kwai-Kolors/Kolors/blob/master/kolors/pipelines/pipeline_stable_diffusion_xl_chatglm_256_inpainting.py | ./kolors/pipelines/pipeline_stable_diffusion_xl_chatglm_256_inpainting.py | https://raw.githubusercontent.com/CompVis/latent-diffusion/main/data/inpainting_examples/overture-creations-5sI6fQgYIuo_mask.png | examples image |
| 开源代码引入 | https://github.com/Kwai-Kolors/Kolors/blob/master/dreambooth/train_dreambooth_lora.py | ./dreambooth/train_dreambooth_lora.py | https://www.tensorflow.org/tensorboard | help for args |
| 开源代码引入 | https://github.com/Kwai-Kolors/Kolors/blob/master/dreambooth/train_dreambooth_lora.py | ./dreambooth/train_dreambooth_lora.py | https://pytorch.org/docs/stable/notes/cuda.html#tensorfloat-32-tf32-on-ampere-devices | help for args |




