# DB++ for PyTorch

-   [概述](概述.md)
-   [准备训练环境](准备训练环境.md)
-   [开始训练](开始训练.md)
-   [训练结果展示](训练结果展示.md)
-   [版本说明](版本说明.md)

# 概述

## 简述

MMOCR是基于PyTorch和MMDetection的开源工具包，支持众多OCR相关模型，包括文本检测、文本识别和关键信息提取。此外，它支持广泛使用的学术数据集，并提供许多有用的工具，帮助用户探索模型和数据集的各个方面，实现高质量的算法。

- 参考实现：

  ```
  url=https://github.com/open-mmlab/mmocr
  commit_id=26bc4713d4a451ed510a67be0a4fdd9903fd9011
  ```

- 适配昇腾 AI 处理器的实现：

  ```
  url=https://gitee.com/ascend/ModelZoo-PyTorch.git
  code_path=PyTorch/built-in/cv/detection
  ```


# 准备训练环境

## 准备环境

该模型为不随版本演进模型（随版本演进模型范围可在[此处](https://gitee.com/ascend/ModelZoo-PyTorch/blob/master/PyTorch/README.CN.md)查看），未在最新昇腾配套软件中适配验证，您可以：
1. 根据下面提供PyTorch版本在[软件版本配套表](https://gitee.com/ascend/pytorch#%E6%98%87%E8%85%BE%E8%BE%85%E5%8A%A9%E8%BD%AF%E4%BB%B6)中选择匹配的CANN等软件下载使用。
2. 查看[软件版本配套表](https://gitee.com/ascend/pytorch#%E6%98%87%E8%85%BE%E8%BE%85%E5%8A%A9%E8%BD%AF%E4%BB%B6)后确认对该模型有新版本PyTorch和CANN中的适配需求，请在[modelzoo/issues](https://gitee.com/ascend/modelzoo/issues)中提出您的需求。**自行适配不保证精度和性能达标。**

- 当前模型支持的 PyTorch 历史版本和已知三方库依赖如下表所示。

  **表 1**  版本支持表

  | Torch_Version      | 三方库依赖版本                                 |
  | :--------: | :----------------------------------------------------------: |
  | PyTorch 1.11 | torchvision==0.12.0 |
  | PyTorch 2.1 | torchvision==0.16.0 |
  | PyTorch 2.2 | torchvision==0.17.0 |
  | PyTorch 2.3 | torchvision==0.18.1 |
  | PyTorch 2.4 | torchvision==0.19.0 |
  
- 环境准备指导。

  请参考《[Pytorch框架训练环境准备](https://www.hiascend.com/document/detail/zh/ModelZoo/pytorchframework/ptes)》。
  
- 安装依赖。

  首先请卸载环境上已安装的mmcv，mmcv-full，mmdet，mmocr。并在模型源码包根目录下执行以下命令，安装模型对应PyTorch版本需要的依赖。
  ```
  pip install -r 1.11_requirements.txt  # PyTorch1.11版本
  pip install -r 2.1_requirements.txt  # PyTorch2.1版本
  pip install -r 2.2_requirements.txt  # PyTorch2.2版本
  pip install -r 2.3_requirements.txt  # PyTorch2.3版本
  pip install -r 2.4_requirements.txt  # PyTorch2.4版本
  ```
 
  如果pytorch版本大于等于2.0，请参考[链接](http://download.openmmlab.com/mmcv/dist/npu/torch1.8.0/index.html)下载相应架构mmcv的wheel包
  并参考如下步骤处理
  ```
  mv mmcv_full-1.7.0-cp37-cp37m-linux_aarch64.whl.whl mmcv_full-1.7.0-cp37-cp37m-linux_aarch64.zip
  unzip mmcv_full-1.7.0-cp37-cp37m-linux_aarch64.zip
  cd mmcv
  mkdir mmcv
  mv * mmcv/
  cp ../mmcv_need/setup.py ./
  # 安装mmcv
  MMCV_WITH_OPS=1 pip3 install -e . 
  ```
  如果pytorch版本小于2.0, 按如下方法安装mmcv 
  ``` 
  pip install mmcv-full -f http://download.openmmlab.com/mmcv/dist/npu/torch1.8.0/index.html
  ```

  安装mmcv 
  pip install mmdet==2.28.0
  ```
  > **说明：** 
  >只需执行一条对应的PyTorch版本requirements.txt安装命令。
  
- 请注意在x86下开启cpu性能模式 [将cpu设置为performance模式](https://gitee.com/ascend/pytorch/blob/master/docs/zh/PyTorch%E8%AE%AD%E7%BB%83%E8%B0%83%E4%BC%98&%E5%B7%A5%E5%85%B7%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97/PyTorch%E8%AE%AD%E7%BB%83%E8%B0%83%E4%BC%98&%E5%B7%A5%E5%85%B7%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97.md#%E5%B0%86cpu%E8%AE%BE%E7%BD%AE%E4%B8%BAperformance%E6%A8%A1%E5%BC%8F)
  


## 准备数据集

1. 获取数据集。

   请用户自行获取原始数据集**icdar2015**，将数据集上传到服务器任意路径一下并解压，数据集目录结构参考如下所示。
   
   ```
   ├─icdar2015
        ├── annotations
        ├── imgs
        ├── instances_test.json
        └── instances_training.json
   ```

   > **说明：** 
   >该数据集的训练过程脚本只作为一种参考示例。

## 获取预训练模型

请用户自行下载预训练模型**dbnetpp_r50dcnv2_fpnc_100k_iter_synthtext-20220502-db297554.pth**，将预训练模型重命名为**res50dcnv2_synthtext.pth**，并在源码包根目录下新建目录层级`./checkpoints/textdet/dbnetpp/`，将预训练模型移动到该目录下。

# 开始训练

## 训练模型

1. 进入解压后的源码包根目录。

   ```
   cd /${模型文件夹名称} 
   ```

2. 运行训练脚本。

   该模型支持单机单卡训练和单机8卡训练。

   - 单机单卡训练

     启动单卡训练。

     ```
     bash ./test/train_full_1p.sh --data_path=/data/xxx/  # 单卡精度

     bash ./test/train_performance_1p.sh --data_path=/data/xxx/  # 单卡性能
     ```
   
   - 单机8卡训练

     启动8卡训练。

     ```
     bash ./test/train_full_8p.sh --data_path=/data/xxx/  # 8卡精度
   
     bash ./test/train_performance_8p.sh --data_path=/data/xxx/  # 8卡性能
     ```

   --data_path参数填写数据集路径，需写到数据集的一级目录。
   
   
   ```
   主要参数：
   --seed                              //随机数种子设置
   --no-validate                       //设置在训练期间是否进行评测
   --load-from                         //权重加载
   --gpu-id                            //训练卡ID设置
   --diff-seed                         //是否在不同RANK上设置不同随机数种子
   ```
   
   训练完成后，权重文件保存在当前路径下，并输出模型训练精度和性能信息。


# 训练结果展示

**表 2**  训练结果展示表

| NAME     | Accuracy-Highest |  samples/s | AMP_Type |
| :-----: | :-----:  | :---: | :------: |
| 1p-竞品A  | Hmean: 0.861 | 26.08 |       O1 |
| 8p-竞品A  | Hmean: 0.861 | 186.4 |       O1 |
| 1p-NPU   | Hmean: 0.861 | 15.3 |       O1 |
| 8p-NPU   | Hmean: 0.861 | 115.66 |       O1 |

# 版本说明

## 变更

2023.03.13：更新readme，重新发布。

2022.11.7：首次发布。

## FAQ

无。

# 公网地址说明

代码涉及公网地址参考 public_address_statement.md
