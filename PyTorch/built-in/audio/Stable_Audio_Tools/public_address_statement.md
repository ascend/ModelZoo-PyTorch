| 类型 | 开源代码地址 | 文件名                                                                 | 公网IP地址/公网URL地址/域名/邮箱地址       | 用途说明   |
| ------- |------|----------------------------|------------------------------|--------|
| 开源代码引入 | https://python-poetry.org/docs/basic-usage/#commit-your-poetrylock-file-to-version-control |.\.gitignore | https://python-poetry.org    | 设置说明   |
| 开源代码引入 | https://pdm.fming.dev/#use-with-ide | .\.gitignore | https://pdm.fming.dev        | 设置说明   |
| 开源代码引入 | https://github.com/github/gitignore/blob/main/Global/JetBrains.gitignore | .\.gitignore| https://github.com           | 设置说明   |
| 开源代码引入 | https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.windows.get_window.html | .\stable_audio_tools\training\losses\auraloss.py | https://docs.scipy.org/      | 源码实现   |
| 开源代码引入 | https://github.com/csteinmetz1/auraloss/blob/main/auraloss/freq.py | .\stable_audio_tools\training\losses\auraloss.py | https://github.com           | 源码实现   |
| 开源代码引入 | https://arxiv.org/abs/1911.08922 | .\stable_audio_tools\training\losses\auraloss.py | https://arxiv.org/           | 参考论文地址 |
| 开源代码引入 | https://arxiv.org/abs/1808.06719 | .\stable_audio_tools\training\losses\auraloss.py | https://arxiv.org/           | 参考论文地址 |
| 开源代码引入 | https://arxiv.org/abs/2001.04643v1 | .\stable_audio_tools\training\losses\auraloss.py | https://arxiv.org/           | 参考论文地址 |
| 开源代码引入 | https://arxiv.org/abs/1904.04472 | .\stable_audio_tools\training\losses\auraloss.py | https://arxiv.org/           | 参考论文地址 |
| 开源代码引入 | https://arxiv.org/abs/1910.11480 | .\stable_audio_tools\training\losses\auraloss.py | https://arxiv.org/           | 参考论文地址 |
| 开源代码引入 | https://arxiv.org/abs/2010.10291 | .\stable_audio_tools\training\losses\auraloss.py | https://arxiv.org/           | 参考论文地址 |
| 开源代码引入 | https://ieeexplore.ieee.org/document/681427 | .\stable_audio_tools\training\losses\auraloss.py | https://ieeexplore.ieee.org/ | 参考论文地址 |


