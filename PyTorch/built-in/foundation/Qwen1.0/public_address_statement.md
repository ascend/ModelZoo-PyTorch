| 类型 | 开源代码地址 | 文件名                                      | 公网IP地址/公网URL地址/域名/邮箱地址                                             | 用途说明 |
| --- | ----------- | ------------------------------------------- | ----------------------------------------------------------------------------- | -------- |
| 开源代码引入 | https://github.com/lm-sys/FastChat/blob/5095615810cf613dba7f27dd155f571fcff976d8/fastchat/data/convert_alpaca.py | Qwen1.0/convert_alpaca.py | / | 源码实现 |
| 开源代码引入 | https://github.com/QwenLM/Qwen/blob/main/finetune/ds_config_zero2.json | Qwen1.0/tests/ds_config_zero2.json | / | 源码实现 |
| 开源代码引入 | https://github.com/QwenLM/Qwen/blob/main/finetune/finetune_ds.sh | Qwen1.0/tests/finetune_ds.sh | / | 源码实现 |
| 开源代码引入 | https://github.com/QwenLM/Qwen/blob/main/finetune.py | Qwen1.0/tests/finetune.py | / | 源码实现 |