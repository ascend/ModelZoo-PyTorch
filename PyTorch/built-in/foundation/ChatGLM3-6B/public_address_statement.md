| 类型 | 开源代码地址 | 文件名 | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明 |
| ---- | ------------ | ------ | ------------------------------------ | -------- |
| 开源代码引入 | https://huggingface.co/THUDM/chatglm3-6b/blob/main/configuration_chatglm.py|model/modeling_chatglm.py	| https://huggingface.co/models?filter=chatglm | 模型相关说明 |
| 开源代码引入 | https://huggingface.co/THUDM/chatglm3-6b/blob/main/configuration_chatglm.py|model/modeling_chatglm.py	| https://github.com/labmlai/annotated_deep_learning_paper_implementations/blob/master/labml_nn/transformers/rope/__init__.py | 模型相关说明 |
| 开源代码引入 | https://huggingface.co/THUDM/chatglm3-6b/blob/main/configuration_chatglm.py|model/modeling_chatglm.py	| https://github.com/labmlai/annotated_deep_learning_paper_implementations/blob/master/license | 模型license相关说明 |
| 开源代码引入 | https://huggingface.co/THUDM/chatglm3-6b/blob/main/configuration_chatglm.py|model/modeling_chatglm.py	| https://arxiv.org/pdf/2002.05202.pdf | 模型相关说明 |
| 开源代码引入 | https://huggingface.co/THUDM/chatglm3-6b/blob/main/configuration_chatglm.py|model/modeling_chatglm.py	| https://huggingface.co/docs/transformers/main/en/main_classes/text_generation | 模型相关说明 |
| 开源代码引入 | https://huggingface.co/THUDM/chatglm3-6b-32k/blob/main/modeling_chatglm.py	| model-32K/modeling_chatglm.py | https://huggingface.co/models?filter=chatglm | 模型相关说明 |
| 开源代码引入 | https://huggingface.co/THUDM/chatglm3-6b-32k/blob/main/modeling_chatglm.py	| model-32K/modeling_chatglm.py | https://github.com/labmlai/annotated_deep_learning_paper_implementations/blob/master/labml_nn/transformers/rope/__init__.py | 模型相关说明 |
| 开源代码引入 | https://huggingface.co/THUDM/chatglm3-6b-32k/blob/main/modeling_chatglm.py	| model-32K/modeling_chatglm.py | https://github.com/labmlai/annotated_deep_learning_paper_implementations/blob/master/license | 模型license相关说明 |
| 开源代码引入 | https://huggingface.co/THUDM/chatglm3-6b-32k/blob/main/modeling_chatglm.py	| model-32K/modeling_chatglm.py |  https://arxiv.org/pdf/2002.05202.pdf | 模型相关说明 |
| 开源代码引入 | https://huggingface.co/THUDM/chatglm3-6b-32k/blob/main/modeling_chatglm.py	| model-32K/modeling_chatglm.py | https://huggingface.co/docs/transformers/main/en/main_classes/text_generation | 模型相关说明 |