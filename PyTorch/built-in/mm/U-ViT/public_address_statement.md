| 类型 | 开源代码地址 | 文件名                                                  | 公网IP地址/公网URL地址/域名/邮箱地址                     | 用途说明 |
| ------- |------|------------------------------------------------------|-------------------------------------------------|------|
| 开源代码引入 | https://github.com/baofff/U-ViT/blob/main/libs/timm.py | \libs\timm.py                                        | https://people.sc.fsu.edu/~jburkardt/presentations/truncated_normal.pdf | 论文地址 |
| 开源代码引入 | https://github.com/baofff/U-ViT/blob/main/libs/timm.py | \libs\timm.py                                        | https://github.com/tensorflow/tpu/issues/494#issuecomment-532968956  | 相关说明 |
| 开源代码引入 | https://github.com/baofff/U-ViT/blob/main/tools/fid_score.py | \tools\fid_score.py                             | https://github.com/bioinf-jku/TTUR | 源码实现 |
| 开源代码引入 | https://github.com/baofff/U-ViT/blob/main/tools/fid_score.py | \tools\fid_score.py | http://www.apache.org/licenses/LICENSE-2.0 | 许可地址 |
| 开源代码引入 | https://github.com/baofff/U-ViT/blob/main/tools/inception.py | \tools\inception.py | http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz | 下载权重 |
| 开源代码引入 | https://github.com/baofff/U-ViT/blob/main/tools/inception.py | \tools\inception.py | https://github.com/mseitzer/pytorch-fid/releases/download/fid_weights/pt_inception-2015-12-05-6726825d.pth | 下载权重 |
| 开源代码引入 | https://github.com/baofff/U-ViT/blob/main/tools/inception.py | \tools\inception.py | https://github.com/mseitzer/pytorch-fid/issues/28 | 设置说明 |
