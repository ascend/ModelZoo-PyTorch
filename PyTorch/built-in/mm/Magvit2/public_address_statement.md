| 类型 | 开源代码地址                                                                | 文件名                                     | 公网IP地址/公网URL地址/域名/邮箱地址 | 用途说明     |
| ------- |-----------------------------------------------------------------------|-----------------------------------------|------------------------|----------|					
|	开源代码引入	| 	https://github.com/lucidrains/magvit2-pytorch/blob/main/setup.py | 	.\setup.py		                           | lucidrains@gmail.com 	                      | 	作者邮箱	   |
|	开源代码引入	| 	https://github.com/lucidrains/magvit2-pytorch/blob/main/.gitignore | 	.\.gitignore		                         | https://python-poetry.org/docs/basic-usage/#commit-your-poetrylock-file-to-version-control 	                      | 	文档地址	   |
|	开源代码引入	| 	https://github.com/lucidrains/magvit2-pytorch/blob/main/.gitignore | 	.\.gitignore		                         | https://pdm.fming.dev/#use-with-ide 	                      | 	文档地址	   |
|	开源代码引入	| 	https://github.com/lucidrains/magvit2-pytorch/blob/main/.gitignore | 	.\.gitignore		                         | https://github.com/github/gitignore/blob/main/Global/JetBrains.gitignore 	                      | 	文档地址	   |
|	开源代码引入	| 	https://github.com/lucidrains/magvit2-pytorch/blob/main/magvit2_pytorch/magvit2_pytorch.py | 	.\magvit2_pytorch\magvit2_pytorch.py		 | https://arxiv.org/abs/2012.13375 	                      | 	参考论文地址	 |
|	开源代码引入	| 	https://github.com/lucidrains/magvit2-pytorch/blob/main/magvit2_pytorch/magvit2_pytorch.py | 	.\magvit2_pytorch\magvit2_pytorch.py		 | https://arxiv.org/abs/2106.09681 	                      | 	参考论文地址	 |

