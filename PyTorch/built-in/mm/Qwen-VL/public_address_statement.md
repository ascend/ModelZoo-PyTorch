
|   类型   |          开源代码地址           |                                              文件名                                              | 公网IP地址/公网URL地址/域名/邮箱 |       用途说明        |
|:------:|:-------------------------:|:---------------------------------------------------------------------------------------------:|:--------------------:|:-----------------:|
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/eval_mm/vqa_eval.py | ./eval_mm/vqa_eval.py | https://opensource.org/licenses/BSD-3-Clause | Modified BSD License |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/eval_mm/vqa_eval.py | ./eval_mm/vqa_eval.py | https://github.com/tylin/coco-caption/blob/master/pycocoevalcap/eval.py | written by Tsung-Yi Lin for MSCOCO Python API |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/web_demo_mm.py |         ./web_demo_mm.py          | https://modelscope.cn/api/v1/models/qwen/Qwen-7B-Chat/repo | Qwen-7B-Chat modelscope地址 |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/web_demo_mm.py | ./web_demo_mm.py | https://modelscope.cn/models/qwen/Qwen-VL/summary | Qwen-VL modelscope地址 |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/web_demo_mm.py | ./web_demo_mm.py | https://huggingface.co/Qwen/Qwen-VL | Qwen-VL modelscope地址 |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/web_demo_mm.py |         ./web_demo_mm.py          | https://modelscope.cn/models/qwen/Qwen-VL-Chat/summary |          Qwen-VL-CHat modelscope地址          |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/web_demo_mm.py | ./web_demo_mm.py | https://huggingface.co/Qwen/Qwen-VL-Chat | Qwen-VL-Chat huggingface地址 |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/web_demo_mm.py | ./web_demo_mm.py | https://github.com/QwenLM/Qwen-VL | QwenLM 官方仓库地址 |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/eval_mm/seed_bench/trans.py |   ./eval_mm/seed_bench/trans.py   | https://huggingface.co/datasets/AILab-CVC/SEED-Bench/blob/main/SEED-Bench.json |            a large-scale benchmark            |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/eval_mm/seed_bench/trans.py |   ./eval_mm/seed_bench/trans.py   | https://github.com/AILab-CVC/SEED-Bench/blob/main/DATASET.md |      readme for a large-scale benchmark       |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/eval_mm/vqa.py |         ./eval_mm/vqa.py          | https://github.com/pdollar/coco/blob/master/PythonAPI/pycocotools/coco.py | written by Tsung-Yi Lin for MSCOCO Python API |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/eval_mm/vqa.py |         ./eval_mm/vqa.py          |         https://opensource.org/licenses/BSD-3-Clause         |             Modified BSD License              |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/eval_mm/evaluate_vqa.py |     ./eval_mm/evaluate_vqa.py     | https://github.com/google-research/pix2struct/blob/main/pix2struct/metrics.py#L81 | source code of google-research/pix2struct |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/eval_mm/evaluate_vqa.py |     ./eval_mm/evaluate_vqa.py     |             https://arxiv.org/pdf/2203.10244.pdf             | ChartQA: A Benchmark for Question Answering about Charts with Visual and Logical Reasoning |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/eval_mm/infographicsvqa_eval.py | ./eval_mm/infographicsvqa_eval.py |        https://www.docvqa.org/datasets/infographicvqa        | InfographicVQA dataset (2021 Challenge, task 3 dataset) |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/eval_mm/infographicsvqa_eval.py | ./eval_mm/infographicsvqa_eval.py |        https://rrc.cvc.uab.es/?ch=17&com=introduction        | Overview - Document Visual Question Answerin |
| 开源代码引入 | https://github.com/QwenLM/Qwen-VL/blob/master/openai_api.py | ./openai_api.py | https://platform.openai.com/docs/api-reference/chat | openAI api reference |
| 开源代码引入 | https://www.modelscope.cn/models/qwen/Qwen-VL-Chat/file/view/master?fileName=visual.py&status=1 | ./models/visual.py | https://github.com/facebookresearch/mae/blob/efb2a8062c206524e35e47d04501ed4f544c0ae8/util/pos_embed.py#L20 | source code of facebookresearch/mae |
|开源代码引入| https://github.com/QwenLM/Qwen-VL/blob/master/README.md | ./infer.py | https://qianwen-res.oss-cn-beijing.aliyuncs.com/Qwen-VL/assets/demo.jpeg | demo image for inference |
