torch==2.0.0
torchaudio==2.0.2
torchvision==0.15.2
diffusers==0.26.3
transformers==4.38.2
open_clip_torch==2.20.0
opencv-python==4.9.0.80