class NpuConfig(object):
    use_cpu = True
    Duo = False
    A2 = False
    use_parallel_inferencing = False
    unet_session = False
    clip_session = False
    vae_session = False
    unet_session_bg = False
