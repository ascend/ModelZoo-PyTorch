python inference.py \
./configs/opensora/inference/16x256x256.py \
--ckpt-path /home/mazhixin/OpenSora-v1-HQ-16x256x256.pth \
--prompt-path ./assets/texts/t2v_samples.txt \
--use_mindie 1 \
--device_id 0