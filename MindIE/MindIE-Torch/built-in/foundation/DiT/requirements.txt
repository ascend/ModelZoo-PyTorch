torch==2.1.0
torchvision==0.16.0
timm==0.9.12
diffusers==0.26.3
accelerate==0.21.0
scipy==1.11.1
pytorch-fid