setuptools==57.5.0
torch==2.1.0
diffusers==0.26.3
transformers==4.46.0
open_clip_torch==2.20.0
onnx==1.15.0