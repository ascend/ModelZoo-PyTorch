# Copyright 2024 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import multiprocessing as mp
import time
from dataclasses import dataclass
from typing import List

import numpy as np
import torch
import mindietorch


@dataclass
class RuntimeIOInfoCache:
    input_shapes: List[tuple]
    input_dtypes: List[type]
    output_shapes: List[tuple]
    output_dtypes: List[type]


class BackgroundRuntimeCache:
    def __init__(
            self,
            device_id: int,
            model_path: str,
            io_info: RuntimeIOInfoCache
    ):
        # Create a pipe for process synchronization
        self.sync_pipe, sync_pipe_peer = mp.Pipe(duplex=True)

        # Create shared buffers
        input_spaces = self.create_shared_buffers(io_info.input_shapes,
                                                  io_info.input_dtypes)
        output_spaces = self.create_shared_buffers(io_info.output_shapes,
                                                   io_info.output_dtypes)

        # Build numpy arrays on the shared buffers
        self.input_arrays = [
            np.frombuffer(b, dtype=t).reshape(s) for (b, s, t) in zip(
                input_spaces, io_info.input_shapes, io_info.input_dtypes)
        ]
        self.output_arrays = [
            np.frombuffer(b, dtype=t).reshape(s) for (b, s, t) in zip(
                output_spaces, io_info.output_shapes, io_info.output_dtypes)
        ]

        mp.set_start_method('fork', force=True)
        self.p = mp.Process(target=self.run_infer,
                            args=[
                                sync_pipe_peer, input_spaces, output_spaces,
                                io_info, device_id, model_path
                            ])
        self.p.start()

        # Wait until the sub process is ready
        self.wait()

    @staticmethod
    def create_shared_buffers(shapes: List[tuple],
                              dtypes: List[type]) -> List[mp.RawArray]:
        buffers = []
        for shape, dtype in zip(shapes, dtypes):
            size = 1
            for x in shape:
                size *= x

            raw_array = mp.RawArray(np.ctypeslib.as_ctypes_type(dtype), size)
            buffers.append(raw_array)

        return buffers

    def infer_asyn(self, feeds: List[np.ndarray], skip) -> None:
        for i, _ in enumerate(self.input_arrays):
            self.input_arrays[i][:] = feeds[i][:]

        if skip:
            self.sync_pipe.send('skip')
        else:
            self.sync_pipe.send('cache')

    def wait(self) -> None:
        self.sync_pipe.recv()

    def get_outputs(self) -> List[np.ndarray]:
        return self.output_arrays

    def wait_and_get_outputs(self) -> List[np.ndarray]:
        self.wait()
        return self.get_outputs()

    def infer(self, feeds: List[np.ndarray]) -> List[np.ndarray]:
        self.infer_asyn(feeds)
        return self.wait_and_get_outputs()

    def stop(self):
        # Stop the sub process
        self.sync_pipe.send('STOP')

    @staticmethod
    def run_infer(
            sync_pipe: mp.connection.Connection,
            input_spaces: List[np.ndarray],
            output_spaces: List[np.ndarray],
            io_info: RuntimeIOInfoCache,
            device_id: int,
            model_path: list,
    ) -> None:
        # The sub process function
        # Create a runtime
        print(f"[info] bg device id: {device_id}")

        # Tell the main function that we are ready
        model_cache = torch.jit.load(model_path[0]).eval()
        model_skip = torch.jit.load(model_path[1]).eval()

        # Build numpy arrays on the shared buffers
        input_arrays = [
            np.frombuffer(b, dtype=t).reshape(s) for (b, s, t) in zip(
                input_spaces, io_info.input_shapes, io_info.input_dtypes)
        ]

        output_arrays = [
            np.frombuffer(b, dtype=t).reshape(s) for (b, s, t) in zip(
                output_spaces, io_info.output_shapes, io_info.output_dtypes)
        ]
        mindietorch.set_device(device_id)

        # Tell the main function that we are ready
        sync_pipe.send('')

        stream = mindietorch.npu.Stream(f"npu:{device_id}")

        return_cache = None

        # Keep looping until recived a 'STOP'
        while True:
            flag = sync_pipe.recv()
            if flag == 'STOP':
                break

            if flag == 'cache':
                sample, timestep, encoder_hidden_states, return_flag = [
                    torch.Tensor(input_array) for input_array in input_arrays
                ]
            else:
                sample, timestep, encoder_hidden_states, return_flag = [
                    torch.Tensor(input_array) for input_array in input_arrays
                ]

            sample_npu = sample.to(torch.float32).to(f"npu:{device_id}")
            timestep_npu = timestep.to(torch.int64).to(f"npu:{device_id}")
            encoder_hidden_states_npu = encoder_hidden_states.to(torch.float32).to(f"npu:{device_id}")
            flag_npu = return_flag.to(torch.int64).to(f"npu:{device_id}")

            if flag == 'cache':
                with mindietorch.npu.stream(stream):
                    output_npu = model_cache(sample_npu, timestep_npu, encoder_hidden_states_npu, flag_npu)
                    stream.synchronize()

                    output_cpu0 = output_npu[0].to('cpu')
                    output0 = output_cpu0.numpy()
                    output_arrays[0][:] = output0

                    return_cache = output_npu[1]
            else:
                with mindietorch.npu.stream(stream):
                    output_npu = model_skip(sample_npu, timestep_npu, encoder_hidden_states_npu, flag_npu, return_cache)
                    stream.synchronize()

                    output_cpu0 = output_npu.to('cpu')
                    output0 = output_cpu0.numpy()
                    output_arrays[0][:] = output0

            sync_pipe.send('')

    @classmethod
    def clone(cls, device_id: int, model_path: str, runtime_info: RuntimeIOInfoCache) -> 'BackgroundRuntimeCache':
        # Get shapes, datatypes from an existed engine,
        # then use them to create a BackgroundRuntimeCache
        return cls(device_id, model_path, runtime_info)
